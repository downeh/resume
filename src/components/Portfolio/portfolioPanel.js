import'./portfolio.css';
import React, { Component } from 'react';
import video from './files/watch.mp4';
import k15 from'./files/k15.png';

class PortfolioPanel extends Component {
  render() {
    return (
    <div><div className="sectionHeader">#PORTFOLIO</div>
    <section className="careerContainer">
    <a href="https://bitbucket.org/downeh/k15t/src/master/app/">k15t - Multiple Source Search Engine with typehead [Personal]</a></section>  
    <section className="careerContainer"><a>Prada Watch - System and business SLA monitoring tool [JPM Confidential]</a></section>  
    <section className="careerContainer"><a>Spider-Web - Re-development of excel based tool utilised into SPA [JPM Confidential]</a></section>
    </div>
    );
  }
}

export default PortfolioPanel;
