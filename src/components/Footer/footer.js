import './footer.css'
import React, { Component } from 'react';
import { Footer, Container, Content } from 'react-bulma-components/full';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBitbucket } from '@fortawesome/free-brands-svg-icons';


class FooterPart extends Component {
  render() {
    return (
    <Footer className="footerContainer">
        <Container>
          <Content>
            <p>
              <strong>GKD</strong>
            </p>
          </Content>
        </Container>
      </Footer>
    );
  }
}
export default FooterPart;