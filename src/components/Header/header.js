import './header.css'
import React, { Component } from 'react';
import { Columns, Heading } from 'react-bulma-components/full';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBitbucket } from '@fortawesome/free-brands-svg-icons';


class Header extends Component {
  render() {
    return (
      <header className="App-header">
        <Columns className="is-hidden-mobile">
          <Columns.Column size="auto">
            <div className="App-title is-6 is-fullhd" device='is-mobile'><b>GRANT<span>DOWNIE</span><br />>RÉSUMÉ</b></div>
          </Columns.Column>
          <Columns.Column>
            <h1 className="bitBucket"><a href="https://bitbucket.org/downeh/" target="_blank"><FontAwesomeIcon size="2x" icon={faBitbucket} /></a></h1>
          </Columns.Column>
        </Columns>
         <Columns className="is-hidden-desktop mobileHeader">
          <Columns.Column size="auto">
            GRANT<b>DOWNIE</b> <b>RÉSUMÉ</b>
          </Columns.Column>
          <Columns.Column>
            <h1 className="bitBucket"><a href="https://bitbucket.org/downeh/" target="_blank"><FontAwesomeIcon size="2x" icon={faBitbucket} /></a></h1>
          </Columns.Column>
        </Columns>
      </header>
    );
  }
}
export default Header;