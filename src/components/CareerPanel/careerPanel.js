import React, { Component } from 'react';
import { Columns, Button, Box, Media, Item, Image, Content, Level } from 'react-bulma-components/full';
import './career.css';
import jpmLogo from'./img/jpm.png';
import hpLogo from'./img/hp.png';

class CareerPanel extends Component {
  render() {
    return (
    <div>
        <div className="sectionHeader">#CAREER</div>
          <Columns>
            <Columns.Column size="half">
                       <Box>
                        <Media>
                          <Media.Item renderAs="figure" position="left">
                            <Image renderas="p" size={64} alt="64x64" src={jpmLogo} />
                          </Media.Item>
                          <Media.Item>
                          <Content> 
                           <blockquote>
                              <p><strong>JPMorgan</strong>
                                <br />
                                <small>FRONTEND DEVELOPER</small>
                              </p>
                            </blockquote>
                                <Button size="small" color="info">BALSAMIQ</Button>
                                <Button size="small" color="light">GIT</Button>
                                <Button size="small" color="info">PHANTOMJS</Button>
                                <Button size="small" color="light">JENKINS</Button>
                                <Button size="small" color="success">SQL</Button>
                                <Button size="small" color="warning">FINANCIAL</Button>
                                <Button size="small" color="info">HTML5</Button>
                                <Button size="small" color="info">CSS3</Button>
                                <Button size="small" color="info">REACT</Button>
                                <Button size="small" color="success">ASE</Button>
                                <Button size="small" color="info">ES6</Button>
                                <Button size="small" color="light">WEBPACK</Button>
                                <Button size="small" color="warning">UNIX</Button>
                                <Button size="small" color="light">NPM</Button>
                                <Button size="small" color="info">D3</Button>
                                <Button size="small" color="primary">CLOUD</Button>
                                <Button size="small" color="primary">SELENIUM</Button>
                                <Button size="small" color="primary">SPECFLOW</Button>
                                <Button size="small" color="danger">UNIX</Button>
                            </Content>
                          </Media.Item>
                        </Media>
                      </Box>
            </Columns.Column>

            <Columns.Column size="half">
                   <Box>
                    <Media>
                      <Media.Item renderAs="figure" position="left">
                        <Image renderas="p" size={64} alt="64x64" src={hpLogo} />
                      </Media.Item>
                      <Media.Item>
                        <Content>
                          <blockquote>
                          <p><strong>Hewlett Packard</strong>
                            <br />
                            <small>SAP TEST MANAGER</small>
                          </p>
                          </blockquote>
                            <Button size="small" color="dark">C# WPF</Button>
                            <Button size="small" color="light">VB.NET</Button>
                            <Button size="small" color="dark">ABAP</Button>
                            <Button size="small" color="warning">UNIX</Button>
                            <Button size="small" color="success">SQL</Button>
                            <Button size="small" color="primary">JQUERY</Button>
                            <Button size="small" color="info">CSS</Button>
                            <Button size="small" color="info">PHOTOSHOP</Button>
                            <Button size="small" color="info">QUICKTEST PRO</Button>
                            <Button size="small" color="success">SAP</Button>
                            <Button size="small" color="success">HPSM</Button>
                            <Button size="small" color="success">ALM</Button>
                            <Button size="small" color="success">SIX SIGMA</Button>
                            <Button size="small" color="primary">SHAREPOINT</Button>
                        </Content>
                       </Media.Item>
                     </Media>
                    </Box>
            </Columns.Column>

          </Columns>
         
        <div className="sectionHeader">#EDUCATION</div>
        <div className="container">
                <Columns>
                     <Columns.Column size="6">
                       <Box>
                        <Media>
                          <Content> 
                              <p><strong>MSc International Business (2012)</strong>
                                <br />
                                <small>Glasgow Caledonian University</small>
                                <br />
                                <small>+ 6 month Erasmus exchange in Arhem, The Netherlands.</small>
                              </p>

                            </Content>
                        </Media>
                      </Box>
                           </Columns.Column>
           
           <Columns.Column size="6">
                       <Box>
                        <Media>
                          <Content> 
                              <p><strong>BSc Computing & Networks (2009)</strong>
                                <br />
                                <small>University of Abertay</small>
                                <br />
                                <small>+ 1 year HP Technical Internship with the SAP Global Supply Chain department.</small>
                                <br />
                                <small>+ 6 Months NCR Internship.</small>
                                <br />
                                <small>+ 1 year Microsoft Student Internship.</small>
                              </p>
                            </Content>
                        </Media>
                      </Box>
                  </Columns.Column>
                </Columns>  
                </div>
        </div> 
    );
  }
}
export default CareerPanel;
