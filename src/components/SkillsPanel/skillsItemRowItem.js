import './skills.css'
import React, { Component } from 'react';
import { Columns } from 'react-bulma-components/full';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircle } from '@fortawesome/free-solid-svg-icons'

class SkillsItemRowItem extends Component {
  constructor(props) {
  super(props);
  this.state = { };
}

componentWillMount() {
  /* When component mounts, take the props and add x number of rating bullet icons. e.g. reating:3 is 3 bullets. 
     Add each new element to an array and on completion of loop set to state */ 
  const providedRating = this.props.rating;

  var arrRatings = [];
  for (let i = 0; i < providedRating; i++) { 
      let text = <FontAwesomeIcon icon={faCircle} size="xs" key={i} className="skillsBullet" />;
      arrRatings.push(text)
  }
  this.setState({ratingList:arrRatings})
}

  render() {
      return (
             <Columns.Column className="skillRating" size="">
              <Columns>
                <Columns.Column size="4">
                  <div className="skillTitle">{this.props.skill}</div>
                </Columns.Column>
                <Columns.Column size="4">
                  {this.state.ratingList}
                </Columns.Column>
              </Columns>
             </Columns.Column>
            );
  }
}
export default SkillsItemRowItem;