import './skills.css';
import React, { Component } from 'react';
import { Columns, Level, Item, Heading } from 'react-bulma-components/full';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlane, faHeart, faComments, faGamepad } from '@fortawesome/free-solid-svg-icons';
import { faInstagram, faLinkedinIn } from '@fortawesome/free-brands-svg-icons';
import { faEnvelope } from '@fortawesome/free-regular-svg-icons';
import mahpuss from'./files/av.jpg';

class SkillsBar extends Component {
  render() {
    return (
        <Columns.Column size="three-thirds" className="personalSidebar">
        <section className="skillsBar">
          <Heading size={5}>#CONTACT</Heading>
          <div className="skillsBarBox">
           <a href="https://www.instagram.com/downeh" target="_blank"><FontAwesomeIcon icon={faInstagram} size="3x" /></a>
          </div>
          <div className="skillsBarBox">
            <a href="https://www.linkedin.com/in/grant-downie-a11a7231" target="_blank"><FontAwesomeIcon icon={faLinkedinIn} size="3x" /></a>
          </div>
        </section>

          <section className="skillsBar">
            <Heading size={5}>#HOBBIES</Heading>
            <div className="skillsBarBox">
              <p><FontAwesomeIcon icon={faPlane} size="3x" /></p>
            </div>
            <div className="skillsBarBox">
              <p><FontAwesomeIcon icon={faComments} size="3x" /></p>
            </div>
            <div className="skillsBarBox">
            <p><FontAwesomeIcon icon={faGamepad} size="3x" /></p></div>
          </section>
        </Columns.Column>
    );
  }
}

export default SkillsBar;
