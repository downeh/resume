import './skills.css';
import React, { Component } from 'react';
import { Columns } from 'react-bulma-components/full';
import './skillPie.css'

class SkillsPies extends Component {
  render() {
    return (
        <section className="skillPieCharts">
        <Columns>
          <Columns.Column size={4}>
            <figure className="chart chartFrontend" data-percent="100">
              <figcaption>Frontend</figcaption>
              <img className="css" />
              <svg width="200" height="200">
                <circle className="outer" cx="95" cy="95" r="85" transform="rotate(-90, 95, 95)"/>
              </svg>
            </figure>
          </Columns.Column>
          <Columns.Column size={4}>
           <figure className="chart chartFrontend" data-percent="100">
              <figcaption>Backend</figcaption>
              <img className="css" />
              <svg width="200" height="200">
                <circle className="outer" cx="95" cy="95" r="85" transform="rotate(-90, 95, 95)"/>
              </svg>
            </figure>
          </Columns.Column>
          <Columns.Column size={3}>
            <figure className="chart chartFrontend" data-percent="100">
              <figcaption>Testing</figcaption>
              <img className="css" />
              <svg width="200" height="200">
                <circle className="outer" cx="95" cy="95" r="85" transform="rotate(-90, 95, 95)"/>
              </svg>
            </figure>
            </Columns.Column>
        </Columns>
        </section>
    );
  }
}
export default SkillsPies;
