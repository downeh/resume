import React, { Component } from 'react';
import './skills.css';
import { Columns, Box  } from 'react-bulma-components/full';
import SkillsBar from './skillsBar';
import SkillsItemRow from './skillsItemRow';
import SkillsPies from './skillsPies';

class SkillsPanel extends Component {
  render() {
    return (
      <Columns>
        <Columns.Column size="8" className="is-hidden-mobile">
          <div className="sectionHeader"><span>#SUMMARY</span></div>
          <SkillsPies />       
       </Columns.Column>
       <Columns>
        <SkillsBar />
       </Columns>
      </Columns>
    );
  }
}
export default SkillsPanel;