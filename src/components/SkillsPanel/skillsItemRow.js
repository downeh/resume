import './skills.css'
import React, { Component } from 'react';
import { Columns } from 'react-bulma-components/full';
import SkillsItemRowItem from './skillsItemRowItem';

class SkillsItemRow extends Component {
  constructor(props) {
  super(props);
  this.state = {  };
}

  render() {
    return (
            <Columns> {/* Load Skill Item - This constructs the row with 3 x columns. It takes props from parent. */ } 
              <SkillsItemRowItem skill={this.props.skill1} rating={this.props.rating1} />
              <SkillsItemRowItem skill={this.props.skill2} rating={this.props.rating2} />
              <SkillsItemRowItem skill={this.props.skill3} rating={this.props.rating3} />
            </Columns>
    );
  }
}

export default SkillsItemRow;
