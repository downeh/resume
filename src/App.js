import React, { Component } from 'react';
import './App.css';
import HeaderPart from './components/Header/header';
import SkillsPanel from './components/SkillsPanel/skillsPanel';
import CareerPanel from './components/CareerPanel/careerPanel';
import PortfolioPanel from './components/Portfolio/portfolioPanel';
import FooterPart from './components/Footer/footer';

class App extends Component {
  render() {
    return (
      <div className="App">
        <HeaderPart />
          <div className="container">
            <SkillsPanel />
          </div>
          <div className="container is-fluid">
            <CareerPanel />
            <PortfolioPanel />
          </div>
          <FooterPart />
      </div>
    );
  }
}

export default App;
