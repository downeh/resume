# resume

# Grant Downie - Basic Resume
Decided to pull together a web-based version of my resume. Also, decided to do it in 24hrs to see how far I got. 

### To run

* You'll need to have [git](https://git-scm.com/) and [node](https://nodejs.org/en/) installed in your system.
* Fork and clone the project:

```
> $ git clone https://downeh@bitbucket.org/downeh/resume.git
```

* Then install the dependencies:

```
> $ npm install
```

* Run development server:

```
> $ npm run start
```

Open the web browser to `http://localhost:3000/`

### To build the production package

```
> $ npm run build
```


# Wireframe
![picture](https://i.imgur.com/BgDdNCI.png)

#Result
![picture](http://i.imgur.com/B06eWRUg.png)